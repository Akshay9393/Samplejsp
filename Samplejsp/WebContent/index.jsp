<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>

<c:set var="myTest" value="testValue"/>
#1:<c:out value="${myTest}" />
#2:<%=pageContext.getAttribute("myTest") %>

<br/><br/>

<c:set var="anotherTest" value="anotherValue" scope="request"/>
#1:<c:out value="${anotherTest}" />
#2:<%=request.getAttribute("anotherTest") %>